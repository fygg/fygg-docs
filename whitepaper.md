# FYgg : A Generic Decentralised Operating System Network

## Introduction

**FYgg** is a **decentralised operating system** protocol managing permissioned **tokens**
that can represent programs, states, assets or even currency units. A token can have children an
defines how they can be **created**, **provided**, **updated** or **destroyed** in a specialized
**pure functionnal programming language**. The protocol *as is* is **usage-agnostic** and unable
to perform any task of it's own except running the *root token* main function, and its power comes
from the ability of tokens to program how them and their children can be used. These programs are
not meant to perform complex calculation, but only to verify the new state is valid.

When created a token is assigned a **unique and definitive ID**, while it also have a **state ID**
changing each time the token is updated to avoid *replay attacks*. Children are stored in a
**Merkle tree** structure, the parent storing the **Merkle root** of the tree while the
**Merkle proof** of the children must be provided when it's used. Thanks to this it's possible to
discard any token while begin able to verify **state updates** (or **transformations**) if all used
tokens and their proofs are provided.

**Transformations** (or *transforms* for short) describes changed in a group of tokens. It list
tokens which are used together and can store additionnal data (such as signatures) which can be used
to verify their validity. A token can be used in multiple transforms if multiple sub-groups of
tokens require different additionnal data. Each of these individual transforms can be invalid but as
a whole become valid. A transform stores a list of transforms it depends on (sub-groups transforms
or child token transforms). These dependencies make a **directed acyclic graph** which can be
used in token programs to verify rules.

By default, many things are not verified automatically and are dangerous unless they are check
manually by token programs. By this construct it's possible to create really complex systems which
could not be created otherwise while allowing to make a globally secure tree. For exemple, it's
possible by default to create a token with a *subtree Merkle root* **without** providing nor
verifying its children. Most tokens will forbid this, but it can be used to import a large amount of
somehow trusted tokens into the tree. The main usage is cross-tree communication with trust coming
from an user-based oracle (detailed later in this document), allowing bandswitch and computation
sharding between multiple independent trees.

## Merkle trees and proofs

Merkle trees allow to store large sets of data. Each element hash is a leaf of a binary tree, and
each node is the hash of the concatenation of its children. Finally, we only store the root of the
tree called a **Merkle root**.

```mermaid
graph TD
    root(root) --> abcd(abcd)
    root --> efgh(efgh)

    abcd --> ab(ab)
    abcd --> cd(cd)

    efgh --> ef(ef)
    efgh --> gh(gh)

    ab --> a(a)
    ab --> b(b)
    cd --> c(c)
    cd --> d(d)
    ef --> e(e)
    ef --> f(f)
    gh --> g(g)
    gh --> h(h)

    a --> A
    b --> B
    c --> C
    d --> D
    e --> E
    f --> F
    g --> G
    h --> H
```

Data is provided with a proof of its presence in the tree called a **Merkle proof**. The proof is
composed of all hashes  necessary to recompute the branch from the data leaf and the root.

```mermaid
graph TD
    root(root) --> abcd(abcd)
    root --> efgh(efgh)

    abcd --> ab(ab)
    abcd --> cd(cd)

    cd --> c(c)
    cd --> d(d)

    c --> C
```

If data is updated, we can compute its new hash and with the previous proof compute the new one.