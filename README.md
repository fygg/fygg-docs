# FYgg Documentation

This repositiory contains documentation of the FYgg ecosystem.

> This documentation is in writing process and is thus not complete nor accurate.

## Licensing

This documentation is licensed under the GNU Free Documentation License (Version 1.3) with code
snippets and exemples under the GNU Affero General Public License (Version 3.0).

See [LICENSE-FDL](LICENSE-FDL) and [LICENSE-AGPL3](LICENSE-AGPL3) for more details.